#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <pb_encode.h>
#include <pb_decode.h>
#include "Example.pb.h"

#ifdef __cplusplus
extern "C" {
#endif

struct compactRioData
{
    short temperature;
    unsigned int uptime;
    unsigned short current;
    unsigned short voltage;
    unsigned short storageAvail;
    unsigned short ramAvail;
    unsigned short cpuUsage;
    unsigned int ipAddress;
    unsigned int remoteGateway;
};

double parseCrioData(compactRioData *data_in)
{
    double test = 0;

    test += data_in->temperature;
    test += data_in->uptime;
    test += data_in->current;
    test += data_in->storageAvail;
    test += data_in->ramAvail;
    test += data_in->cpuUsage;
    test += data_in->ipAddress;
    test += data_in->remoteGateway;

    return test;
}

double addNums(double in1, double in2)
{
    return in1 + in2;
}

double multNums(double in1, double in2)
{
    return in1 * in2;
}

uint16_t getPacket(uint8_t* packet)
{
    uint8_t buffer[256] = {0};
    uint16_t packetSize;
    bool status;

    ExampleMsg msg = {};
    pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));

    msg.value = 1234567;
    msg.number = 18;
    char name[] = "Woo hoo!";
    strncpy(msg.name, name, strlen(name));

    status = pb_encode(&stream, ExampleMsg_fields, &msg);
    packetSize = stream.bytes_written;

    if (packetSize > sizeof(buffer))
    {
        return 5;
    }

    if (!status)
    {
        return 0;
    }

    memcpy(packet, buffer, sizeof(buffer));
    return packetSize;
}

double testPbuf()
{
    /* This is the buffer where we will store our message. */
    uint8_t buffer[128];
    size_t message_length;
    bool status;

    std::ofstream file;
    file.open("testOutput.bin", std::ios::out | std::ios::binary);

    /* Encode our message */
    {
        /* Allocate space on the stack to store the message data.
         *
         * Nanopb generates simple struct definitions for all the messages.
         * - check out the contents of simple.pb.h! */
        ExampleMsg message;

        /* Create a stream that will write to our buffer. */
        pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));

        /* Fill in the lucky number */
        message.value = 13;

        /* Now we are ready to encode the message! */
        status = pb_encode(&stream, ExampleMsg_fields, &message);
        message_length = stream.bytes_written;
        file.write((char*)buffer, sizeof(buffer));
        file.close();

        /* Then just check for any errors.. */
        if (!status)
        {
            // printf("Encoding failed: %s\n", PB_GET_ERROR(&stream));
            return -1;
        }
    }

    /* Now we could transmit the message over network, store it in a file or
     * wrap it to a pigeon's leg.
     */

    /* But because we are lazy, we will just decode it immediately. */

    {
        /* Allocate space for the decoded message. */
        ExampleMsg message;

        /* Create a stream that reads from the buffer. */
        pb_istream_t stream = pb_istream_from_buffer(buffer, message_length);

        /* Now we are ready to decode the message. */
        status = pb_decode(&stream, ExampleMsg_fields, &message);

        /* Check for errors... */
        if (!status)
        {
            // printf("Decoding failed: %s\n", PB_GET_ERROR(&stream));
            return -2;
        }

        /* Print the data contained in the message. */
        // printf("Your lucky number was %d!\n", message.value);
        return message.value;
    }

    return 0;
}

#ifdef __cplusplus
}
#endif
