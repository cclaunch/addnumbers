#include <iostream>
#include <fstream>
#include <stdint.h>
#include "add_numbers.cpp"

int main(int argc, char* argv[])
{
	uint8_t buffer[256];
	for (unsigned int i = 0; i < 256; ++i)
		buffer[i] = 0;

	uint16_t size = getPacket(buffer);
	// testPbuf();
	std::ofstream file;
	file.open("testOutputMain.bin", std::ios::out | std::ios::binary);
	file.write((char*)buffer, 256);
	file.close();

	std::cout << "Size of packet: " << size << std::endl;
	return 0;
}
