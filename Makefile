all: vxworks local windll 

local:
	@mkdir -p build
	@cd build && cmake .. -DBUILD_LOCAL=ON
	@cd build && make

windll:
	@mkdir -p build_windll
	@cd build_windll && cmake .. -DBUILD_WINDLL=ON -DCMAKE_TOOLCHAIN_FILE=../cmake/mingw32.cmake
	@cd build_windll && make

vxworks:
	@mkdir -p build_vxworks
	@cd build_vxworks && cmake .. -DBUILD_VXWORKS=ON -DCMAKE_TOOLCHAIN_FILE=../cmake/powerpc-vxworks-crio.cmake -DCMAKE_CXX_FLAGS="-D__powerpc_"
	@cd build_vxworks && make

clean:
	@rm -rf build*

spotless: clean
	@rm -rf bin lib
