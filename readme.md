AddNumbers
==========

AddNumbers is just a quick tutorial on how to cross compile a C++ dll for use with a National Instruments Compact RIO running vxworks.  This tutorial is done completely in Ubuntu 14.04.1, though it may work in other
debian based linux distributions.

Configuration
-------------

The following packages were installed on the system to make this work:

mingw32:
sudo apt-get install mingw32

gcc-powerpc-wrs-vxworks:
* Add to your /etc/apt/sources.list  
deb http://debian.repo.frc.s3.amazonaws.com jessie main  
* Add maintainer key  
sudo wget http://debian.repo.frc.s3.amazonaws.com/rbmj.gpg.key  
sudo apt-key add rbmj.gpg.key  
* Install the package  
sudo apt-get update  
sudo apt-get install gcc-powerpc-wrs-vxworks  
* Set the WIND_BASE environmental variable (or add to ~/.profile):  
export WIND_BASE=/usr/powerpc-wrs-vxworks/wind_base  

Build
-----

Simple type make at the root directory to build all libraries.  The Makefile takes care of the cmake options for you.  There are also tags to make windll or vxworks, as well as clean & spotless.


**By Hand**
*Windows DLL*  
cmake .. -DBUILD_WINDLL=ON -DCMAKE_TOOLCHAIN_FILE=../cmake/mingw32.cmake
make

*VxWorks out*  
cmake .. -DBUILD_VXWORKS=ON -DCMAKE_TOOLCHAIN_FILE=../cmake/powerpc-vxworks-crio.cmake -DCMAKE_CXX_FLAGS="-D__powerpc_"
make
